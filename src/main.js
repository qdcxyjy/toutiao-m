import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant from 'vant'
import 'vant/lib/index.css'
// 引入全局样式
import './styles/index.less'
// 导入字体图标样式
import './styles/iconfont.css'
// 自适应html字体大小 动态设置rem
import 'amfe-flexible'
// 时间过滤器
import '@/utils/dayjs.js'
Vue.use(Vant)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
