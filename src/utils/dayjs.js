// var dayjs = require('dayjs')
import dayjs from 'dayjs'
import 'dayjs/locale/zh-cn' // ES 2015
// import Vue from 'vue'
import relativeTime from 'dayjs/plugin/relativeTime'
dayjs.extend(relativeTime)
// 使用语言包
dayjs.locale('zh-cn') // 全局使用
dayjs().format('YY-M-DD') // ES 2015
console.log(dayjs().format('YY-M-DD'))
// 定义全局过滤器
export const formatDate = (data) => {
  // 过滤器函数怎么拿到要过滤的数据：第一个形参
  return dayjs().to(data)
}

export const nameFormat = () => {
  return '***'
}
// 格式化绝对时间
export const absoluteFormatDate = (date, format) => {
  return dayjs(date).format(format)
}
