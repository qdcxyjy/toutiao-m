/**
 * 封装 axios 请求模块
 */
import axios from 'axios'
import store from '@/store'
import JSONBig from 'json-bigint'

const request = axios.create({
  // baseURL: 'http://ttapi.research.itcast.cn/' // 基础路径
  baseURL: 'http://toutiao-app.itheima.net/',
  transformResponse: [function (data) {
    // data是后台返回的原始数据
    try {
      return JSONBig.parse(data)
    } catch (err) {
      return data
    }
  }]
})

request.interceptors.request.use(config => {
  const { user } = store.state
  if (user && user.token) {
    config.headers.Authorization = 'Bearer ' + user.token
  }
  return config
})

export default request
