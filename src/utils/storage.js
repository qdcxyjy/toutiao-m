// 关于本地存储的二次封装

// 获取本地数据
export const getItem = key => {
  const data = localStorage.getItem(key)
  // data 有可能是 普通字符串
  // data 也可能是 json字符串
  try {
    return JSON.parse(data)
  } catch (err) {
    return data
  }
}

// 存储本地数据
export const setItem = (key, value) => {
  if (typeof value === 'object') {
    value = JSON.stringify(value)
  }
  localStorage.setItem(key, value)
}

// 移除本地数据
export const removeItem = key => {
  localStorage.removeItem(key)
}
