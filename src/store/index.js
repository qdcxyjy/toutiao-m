import Vue from 'vue'
import Vuex from 'vuex'

import { getItem, setItem } from '@/utils/storage.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 方便判断是否为空
    user: getItem('HMTT_TOKEN') // 存储用户token数据
  },
  mutations: {
    // 初始化user数据
    initUser (state, data) {
      state.user = data
      // 本地存储
      setItem('HMTT_TOKEN', data)
    }
  },
  actions: {
  },
  modules: {
  }
})
