// 封装所有文章相关的请求
import request from '@/utils/request.js'

/**
 *获取文章列表
 * @param {*} params { channel_id,timestamp,with_top }
 * @returns
 */
export const getArticleList = params => {
  return request({
    method: 'GET',
    url: 'v1_1/articles',
    params
  })
}

/**
 *获取文章详情
 * @param { Number } id 文章id
 * @returns prtomise
 */
export const getArticleInfo = id => {
  return request({
    method: 'GET',
    url: 'v1_0/articles/' + id
  })
}

/**
 * 收藏文章
 * @param {*} id 用户id
 * @returns promise
 */
export const addArticleCollected = id => {
  return request({
    method: 'POST',
    url: 'v1_0/article/collections',
    data: {
      target: id
    }
  })
}

/**
 * 取消收藏文章
 * @param {*} id 用户id
 * @returns promise
 */
export const deleteArticleCollected = id => {
  return request({
    method: 'DELETE',
    url: 'v1_0/article/collections/' + id
  })
}

/**
 * 点赞文章
 * @param {*} id  文章id
 * @returns
 */
export const addArticleLike = id => {
  return request({
    method: 'POST',
    url: '/v1_0/article/likings',
    data: {
      target: id
    }
  })
}

/**
 * 取消点赞
 * @param {*} id  文章id
 * @returns
 */
export const delArticleLike = id => {
  return request({
    method: 'DELETE',
    url: '/v1_0/article/likings/' + id
  })
}
