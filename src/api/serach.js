// 封装所有搜索道相关的请求
import request from '@/utils/request.js'

/**
 *获取搜索建议
 * @param {*} q 联想的数据
 * @returns promise
 */
export const getSearchSuggestions = q => {
  return request({
    method: 'GET',
    url: 'v1_0/suggestion',
    params: {
      q
    }
  })
}

/**
 *获取搜索结果
 * @params {*} params { page,per_page,q }
 * @returns promise
 */

export const getSearchResults = params => {
  return request({
    method: 'GET',
    url: 'v1_0/search',
    params
  })
}
