// 封装所有评论相关的请求
import request from '@/utils/request.js'

/**
 * 获取评论列表
 * @param {*} params
 * @returns promise
 */
export const getCommentList = params => {
  return request({
    method: 'GET',
    url: '/v1_0/comments',
    params
  })
}

/**
 * 点赞评论
 * @param {*} id 评论id
 * @returns
 */
export const addLike = id => {
  return request({
    method: 'POST',
    url: '/v1_0/comment/likings',
    data: {
      target: id
    }
  })
}

/**
 * 取消点赞
 * @param {*} id 评论id
 * @returns
 */
export const delLike = id => {
  return request({
    method: 'DELETE',
    url: '/v1_0/comment/likings/' + id
  })
}

/**
 * 发布评论
 * @param {*} data
 * {
 * target 评论目标id (文章id或者评论id)
 * content 评论内容
 * art_id 回复的评论所属的文章id
 * }
 * @returns promise
 */
export const postComment = data => {
  return request({
    method: 'POST',
    url: '/v1_0/comments',
    data
  })
}
