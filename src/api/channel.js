// 封装所有频道相关的请求
import request from '@/utils/request.js'

export const getAllChannels = () => {
  return request({
    method: 'GET',
    url: 'v1_0/channels'
  })
}

/**
 *添加频道
 * @param {*} obj { id:频道id, seq:序号 }
 * @returns
 */
export const addUserChannels = obj => {
  return request({
    method: 'PATCH',
    url: 'v1_0/user/channels',
    data: {
      channels: [obj]
    }
  })
}

/**
 *删除频道
 * @param {*} target 删除频道的id
 * @returns promise
 */
export const delUserChannels = target => {
  return request({
    method: 'DELETE',
    url: 'v1_0/user/channels/' + target
  })
}
