// 封装所有用户相关的请求
import request from '@/utils/request.js'
// 在组件中通过this.$store 访问vuex的实例
// 在普通js中通过import 引入store/index.js 能够访问到vuex的实例
// import store from '@/store'
/**
 * 登录请求
 * @param { Object } data { mobile: '', code: ''}
 */
export const login = data => {
  return request({
    method: 'POST',
    url: 'v1_0/authorizations',
    data
  })
}
/**
 *获取手机验证码
 * @param {*} mobile 手机号
 * @returns
 */
export const sendCode = mobile => {
  return request({
    method: 'GET',
    url: 'v1_0/sms/codes/' + mobile
  })
}
/**
 * 获取用户信息
 * @returns
 */
export const getUserProfile = () => {
  return request({
    method: 'GET',
    url: 'v1_0/user'
  })
}

/**
 * 获取用户频道列表
 * @returns
 */
export const getUserChannels = () => {
  return request({
    method: 'GET',
    url: 'v1_0/user/channels'
  })
}

/**
 * 取消关注
 * @param {*} id 用户id
 * @returns promise
 */
export const deleteUserFollowed = id => {
  return request({
    method: 'DELETE',
    url: '/v1_0/user/followings/' + id
  })
}

/**
 * 取消关注
 * @param {*} id 用户id
 * @returns
 */
export const addUserFollowed = id => {
  return request({
    method: 'POST',
    url: '/v1_0/user/followings',
    data: {
      target: id
    }
  })
}

/**
 * 获取用户个人信息
 * @returns promise
 */
export const getUserInfo = () => {
  return request({
    method: 'GET',
    url: '/v1_0/user/profile'
  })
}

/**
 *修改用户信息
  @param {*} data
 * @returns
 */
export const setUserProfile = data => {
  return request({
    method: 'PATCH',
    url: '/v1_0/user/profile',
    data
  })
}
/**
 * 更新头像
 */
export const saveAvatar = data => {
  return request({
    method: 'PATCH',
    url: '/v1_0/user/photo',
    data
  })
}
