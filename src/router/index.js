import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('@/views/layout'),
    children: [
      // path为空 默认子路由
      {
        path: '',
        component: () => import('@/views/home')
      },
      {
        path: '/video',
        component: () => import('@/views/video')
      },
      {
        path: '/qa',
        component: () => import('@/views/qa')
      },
      {
        path: '/profile',
        component: () => import('@/views/profile')
      }
    ]
  },
  {
    path: '/user',
    component: () => import('@/views/user')
  },
  {
    path: '/login',
    component: () => import('@/views/login')
  },
  {
    path: '/article/:articleId',
    props: true, // 开启props获取参数
    component: () => import('@/views/article')
  },
  {
    path: '/search',
    component: () => import('@/views/search')
  }
]

const router = new VueRouter({
  routes
})

export default router
