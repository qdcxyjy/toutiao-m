// postcss
// PostCSS是一个用JavaScript工具箱
module.exports = {
  plugins: {
    // postcss-pxtorem 插件的版本需要 >= 5.0.0
    'postcss-pxtorem': {
      rootValue ({ file }) {
        return file.indexOf('vant') !== -1 ? 37.5 : 75
      }, // 将多少px 转化成rem
      propList: ['*'] // 配置将哪些css属性转换成rem * 所有
    }
  }
}
